package com.example.rocket;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Arrays;

public class Sensors implements SensorEventListener {

    private Comms mSocket;
    private SensorManager sensorManager;
    double ax,ay,az;   // these are the acceleration in x,y and z axis

    public Sensors(SensorManager msensorManager, Comms socket) {

        mSocket = socket;

        sensorManager = msensorManager;
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_NORMAL);

//        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ALL), SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Log.d("Sensor", event.sensor.getName() + ' '  + event.sensor.getStringType() + ' ' + Arrays.toString(event.values));

        mSocket.sendMessage(event.sensor.getName() + ' '  + event.sensor.getStringType() + ' '  +  Arrays.toString(event.values));

//        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
//            ax=event.values[0];
//            ay=event.values[1];
//            az=event.values[2];
//
//            Log.d("ACC", Double.toString(ax) + ' ' + Double.toString(ay) + ' ' + Double.toString(az));
//
//            mSocket.sendMessage("ACC" + Double.toString(ax) + ' ' + Double.toString(ay) + ' ' + Double.toString(az));
//
//        } else if (event.sensor.getType()==Sensor.TYPE_GYROSCOPE) {
//            ax=event.values[0];
//            ay=event.values[1];
//            az=event.values[2];
//
//            Log.d("GYR", Double.toString(ax) + ' ' + Double.toString(ay) + ' ' + Double.toString(az));
//
//            mSocket.sendMessage("GYR" + Double.toString(ax) + ' ' + Double.toString(ay) + ' ' + Double.toString(az));
//        }
    }

}
