package com.example.rocket;

import android.os.Build;
import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class Comms {
    private URI uri;
    private int port = 3000;
    private WebSocketClient mWebSocketClient;

    public Comms (String address){

        try {
            uri = new URI("ws://" + address + ":" + port + "/");
//            uri = new URI("ws://188.37.36.150:8765/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri, new Draft_17()) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
                mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
            }

            @Override
            public void onMessage(String s) {

                Log.d("Websocket new recieved", s);

                byte[] buffer = s.getBytes();

            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        try {
            mWebSocketClient.connectBlocking();
        } catch (Exception e) {
            Log.d("Websocket error", "Error \n\n" + e.getMessage() + "\n\n");
        }
    }

    public void sendMessage(String s) {

        Log.i("Websocket enviar", s);

        if(mWebSocketClient.getConnection().isOpen()){
            mWebSocketClient.send(s);
        } else {
            Log.d("Websocket fechada", "deu coco");
        }
    }

    public void close(){
        mWebSocketClient.getConnection().close();
        mWebSocketClient.close();
    }
}
