import asyncio
import socket
import urllib.request

import websockets

port = 3000

external_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')


async def hello(websocket, path):

    print('New connection')

    while True:
        msg = await websocket.recv()

        print(f"> {msg}")

        # greeting = f"Hello {msg}!"
        #
        # await websocket.send(greeting)
        # print(f"< {greeting}")

start_server = websockets.serve(hello, socket.gethostbyname(socket.gethostname()), port)

print('Running on ' + socket.gethostbyname(socket.gethostname()) + ':' + str(port))
print("Machine's external IP is " + external_ip)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
